package ar.edu.unlu.cursos.spring.damas.service;

//import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import ar.edu.unlu.cursos.spring.damas.entity.Partida;
import ar.edu.unlu.cursos.spring.damas.entity.Usuario;

public interface PartidasService {

	Optional<Partida> buscarId(long id);

	List<Partida> traerTodasPorFechaDescendiente();
	
//	List<Partida> buscarPorFechaHoraAntes(ZonedDateTime fechaHora);
//
//	List<Partida> buscarPorFechaHoraDespues(ZonedDateTime fechaHora);
//
//	List<Partida> buscarPorDuracionMenor(long duracion);
//
//	List<Partida> buscarPorDuracionMayor(long duracion);

	List<Partida> buscarPorUsuario(Usuario usuario);

//	int contarMovimientos(Partida partida);

	@Transactional
	void insertar(Partida partida);

	@Transactional
	void actualizar(Partida partida);

	@Transactional
	void eliminar(long id);

}
