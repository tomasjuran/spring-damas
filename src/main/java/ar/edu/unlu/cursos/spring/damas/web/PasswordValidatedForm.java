package ar.edu.unlu.cursos.spring.damas.web;


public interface PasswordValidatedForm {
	public String getPassword();
	public String getConfirmPassword();
}
