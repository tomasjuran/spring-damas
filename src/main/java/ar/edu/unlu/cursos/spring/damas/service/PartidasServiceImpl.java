package ar.edu.unlu.cursos.spring.damas.service;

//import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import ar.edu.unlu.cursos.spring.damas.entity.Partida;
import ar.edu.unlu.cursos.spring.damas.entity.Usuario;
import ar.edu.unlu.cursos.spring.damas.repository.PartidasRepository;

@Service
public class PartidasServiceImpl implements PartidasService {

	private final PartidasRepository partidasRepository;
	
	@Inject
	public PartidasServiceImpl(PartidasRepository partidasRepository) {
		this.partidasRepository = partidasRepository;
	}
	
	@Override
	public Optional<Partida> buscarId(long id) {
		return partidasRepository.findById(id);
	}
	
	@Override
	public List<Partida> traerTodasPorFechaDescendiente() {
		return partidasRepository.findAllByOrderByFechaHoraDesc();
	}

//	@Override
//	public List<Partida> buscarPorFechaHoraAntes(ZonedDateTime fechaHora) {
//		return partidasRepository.findByFechaHoraBefore(fechaHora);
//	}
//
//	@Override
//	public List<Partida> buscarPorFechaHoraDespues(ZonedDateTime fechaHora) {
//		return partidasRepository.findByFechaHoraAfter(fechaHora);
//	}
//
//	@Override
//	public List<Partida> buscarPorDuracionMenor(long duracion) {
//		return partidasRepository.findByDuracionLessThan(duracion);
//	}
//
//	@Override
//	public List<Partida> buscarPorDuracionMayor(long duracion) {
//		return partidasRepository.findByDuracionGreaterThan(duracion);
//	}

	@Override
	public List<Partida> buscarPorUsuario(Usuario usuario) {
		return partidasRepository.findByBlancasOrNegras(usuario, usuario);
	}
	
//	@Override
//	public int contarMovimientos(Partida partida) {
//		return partidasRepository.countByMovimientos(partida);
//	}
	
	@Transactional
	@Override
	public void insertar(Partida partida) {
		partidasRepository.save(partida);
	}

	@Transactional
	@Override
	public void actualizar(Partida partida) {
		partidasRepository.save(partida);
	}

	@Transactional
	@Override
	public void eliminar(long id) {
		partidasRepository.deleteById(id);
	}

}
