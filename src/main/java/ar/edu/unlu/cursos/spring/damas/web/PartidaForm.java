package ar.edu.unlu.cursos.spring.damas.web;

import java.time.ZonedDateTime;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import ar.edu.unlu.cursos.spring.damas.entity.AuditableBaseEntity;
import ar.edu.unlu.cursos.spring.damas.entity.Movimiento;
import ar.edu.unlu.cursos.spring.damas.entity.Partida;
import ar.edu.unlu.cursos.spring.damas.entity.Partida.Ganador;
import ar.edu.unlu.cursos.spring.damas.entity.Usuario;

public class PartidaForm extends AuditableBaseEntity {

	public enum GanadorView {
		BLANCAS("matches.winner.white"),
		NEGRAS("matches.winner.black"),
		EMPATE("matches.winner.draw"),
		NADIE("matches.winner.none");
		
		private String message;

		private GanadorView(String message) {
			this.message = message;
		}
		
		public String getMessage() {
			return message;
		}
		
		public Ganador getGanador() {
			return Ganador.valueOf(this.name());
		}
		
		public static GanadorView of(Ganador ganador) {
			return GanadorView.valueOf(ganador.name());
		}
	}
	
	public PartidaForm() {
		super();
	}

	public PartidaForm(Partida partida) {
		super();
		this.id = partida.getId();
		this.descripcion = partida.getDescripcion();
		this.blancas = partida.getBlancas();
		this.negras = partida.getNegras();
		this.ganador = partida.getGanador();
		this.fechaHora = partida.getFechaHora();
		this.duracion = partida.getDuracion();
		this.movimientos = partida.getMovimientos();
	}

	private Long id;

	@Size(max = 450)
	private String descripcion;

	@NotNull
	private Usuario blancas;

	@NotNull
	private Usuario negras;

	private Ganador ganador;

	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private ZonedDateTime fechaHora;

	private Long duracion;

	private List<Movimiento> movimientos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Usuario getBlancas() {
		return blancas;
	}

	public void setBlancas(Usuario blancas) {
		this.blancas = blancas;
	}

	public Usuario getNegras() {
		return negras;
	}

	public void setNegras(Usuario negras) {
		this.negras = negras;
	}

	public Ganador getGanador() {
		return ganador;
	}

	public void setGanador(Ganador ganador) {
		this.ganador = ganador;
	}

	public ZonedDateTime getFechaHora() {
		return fechaHora;
	}
	
	public void setFechaHora(ZonedDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Long getDuracion() {
		return duracion;
	}

	public void setDuracion(Long duracion) {
		this.duracion = duracion;
	}

	public List<Movimiento> getMovimientos() {
		return movimientos;
	}

	public void setMovimientos(List<Movimiento> movimientos) {
		this.movimientos = movimientos;
	}
	
	public boolean isNueva() { 
		return id == null;
	}

}
