package ar.edu.unlu.cursos.spring.damas.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import ar.edu.unlu.cursos.spring.damas.entity.Movimiento;

@Component
public class StringToMovimientoListConverter implements Converter<String, List<Movimiento>> {

	@Override
	public List<Movimiento> convert(String source) {
		List<Movimiento> movimientos = new ArrayList<>();
		String[] movArray = source.split(" ");
		for (String mov : movArray) {
			movimientos.add(new Movimiento(mov));
		}
		return movimientos;
	}

	
	
}
