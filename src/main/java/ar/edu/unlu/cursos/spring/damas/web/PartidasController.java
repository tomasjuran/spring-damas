package ar.edu.unlu.cursos.spring.damas.web;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ar.edu.unlu.cursos.spring.damas.entity.Partida;
import ar.edu.unlu.cursos.spring.damas.entity.Usuario;
import ar.edu.unlu.cursos.spring.damas.service.PartidasService;
import ar.edu.unlu.cursos.spring.damas.service.UsuariosService;

@Controller
@RequestMapping(value = "/partidas")
public class PartidasController {
	
	private final UsuariosService usuariosService;
	private final PartidasService partidasService;

	@Inject
	public PartidasController(UsuariosService usuariosService, PartidasService PartidasService) {
		super();
		this.usuariosService = usuariosService;
		this.partidasService = PartidasService;
	}

	@RequestMapping("")
	public String partidas(Model model) {
		List<Partida> partidas = partidasService.traerTodasPorFechaDescendiente();
		model.addAttribute("partidas", partidas);
		return "partidas";
	}
	
	@RequestMapping("/buscar")
	public String buscar(Model model, 
			@RequestParam(required = false) Long userId,
			@RequestParam(required = false) String username) {
		
		Usuario usuario = null;
		if (userId != null) {
			usuario = usuariosService.buscarId(userId).orElse(null);
		}
		if (usuario == null && username != null) {
			usuario = usuariosService.buscarNombre(username).orElse(null);
		}
		List<Partida> partidas = partidasService.buscarPorUsuario(usuario);
		model.addAttribute("usuario", usuario);
		model.addAttribute("partidas", partidas);
		return "partidas";
	}

	@RequestMapping("/{partidaId}")
	public String verPartida(Model model,
			@PathVariable("partidaId") long partidaId) {
		Optional<Partida> partida = partidasService.buscarId(partidaId);
		model.addAttribute("partida", partida.orElse(null));
		return "partida-ver";
	}

	@RequestMapping("/nueva")
	public String nuevaPartida(Model model) {
		model.addAttribute("partidaForm", new PartidaForm());
		model.addAttribute("usuarios", usuariosService.traerTodosPorNombreAlfabeticamente());
		return "partida-editar";
	}

	@RequestMapping(value = "/nueva", method = RequestMethod.POST)
	public String nuevaPartida(Model model,
			@ModelAttribute("PartidaForm") @Valid PartidaForm partidaForm,
			BindingResult result) {
		if (result.hasErrors()) {
			return "partida-editar";
		}

		Partida partida = new Partida();
		partida.setFechaHora(partidaForm.getFechaHora());
		partida.setDescripcion(partidaForm.getDescripcion());
		partida.setBlancas(partidaForm.getBlancas());
		partida.setNegras(partidaForm.getNegras());
		partida.setGanador(partidaForm.getGanador());
		partida.setFechaHora(partidaForm.getFechaHora());
		partida.setDuracion(partidaForm.getDuracion());
		partida.setMovimientos(partidaForm.getMovimientos());
		
		partidasService.insertar(partida);
		return "redirect:/partidas/" + partida.getId();
	}

	@RequestMapping("/{partidaId}/editar")
	public String editarPartida(Model model,
			@PathVariable("partidaId") long partidaId) {
		Partida partida = partidasService.buscarId(partidaId)
				.orElseThrow(RuntimeException::new);
		model.addAttribute("partidaForm", new PartidaForm(partida));
		model.addAttribute("usuarios", usuariosService.traerTodosPorNombreAlfabeticamente());
		return "partida-editar";
	}

	@RequestMapping(value = "/{partidaId}/editar", method = RequestMethod.POST)
	public String editarPartida(
			Model model,
			@PathVariable("partidaId") long partidaId,
			@ModelAttribute("partidaForm") @Valid PartidaForm partidaForm,
			BindingResult result) {

		Partida partida = partidasService.buscarId(partidaId)
				.orElseThrow(RuntimeException::new);

		if (result.hasErrors()) {
			return "partida-editar";
		}

		partida.setFechaHora(partidaForm.getFechaHora());
		partida.setDescripcion(partidaForm.getDescripcion());
		partida.setBlancas(partidaForm.getBlancas());
		partida.setNegras(partidaForm.getNegras());
		partida.setGanador(partidaForm.getGanador());
		partida.setFechaHora(partidaForm.getFechaHora());
		partida.setDuracion(partidaForm.getDuracion());
		if (!partida.getMovimientos().equals(partidaForm.getMovimientos())) {
			partida.setMovimientos(partidaForm.getMovimientos());
		}
		partidasService.actualizar(partida);
		return "redirect:/partidas/" + partida.getId();
	}

	@RequestMapping(value = "/{partidaId}/eliminar")
	public String eliminarPartida(Model model,
			@PathVariable("partidaId") long partidaId) {
		Partida partida = partidasService.buscarId(partidaId)
				.orElseThrow(RuntimeException::new);
		model.addAttribute(partida);
		return "partida-eliminar";
	}

	@RequestMapping(value = "/{partidaId}/eliminar",
			method = RequestMethod.POST)
	public String eliminarPartida(@PathVariable("partidaId") long partidaId) {
		partidasService.buscarId(partidaId).orElseThrow(RuntimeException::new);
		partidasService.eliminar(partidaId);
		return "redirect:/partidas/";
	}
}
