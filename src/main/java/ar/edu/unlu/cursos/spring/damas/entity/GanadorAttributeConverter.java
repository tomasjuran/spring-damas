package ar.edu.unlu.cursos.spring.damas.entity;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import ar.edu.unlu.cursos.spring.damas.entity.Partida.Ganador;

@Converter(autoApply = true)
public class GanadorAttributeConverter implements AttributeConverter<Ganador, Integer> {

	@Override
	public Integer convertToDatabaseColumn(Ganador arg0) {
		return arg0.getValue();
	}

	@Override
	public Ganador convertToEntityAttribute(Integer arg0) {
		return Ganador.of(arg0);
	}

}
