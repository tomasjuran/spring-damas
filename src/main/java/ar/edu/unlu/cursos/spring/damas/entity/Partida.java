package ar.edu.unlu.cursos.spring.damas.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "partidas")
@EntityListeners(AuditingEntityListener.class)
public class Partida extends AuditableBaseEntity implements Serializable {

	private static final long serialVersionUID = 2877342104552711648L;

	/**
	 * BLANCAS == 0<br>
	 * NEGRAS == 1<br>
	 * EMPATE == 2<br>
	 * NADIE == 3<br>
	 */
	public enum Ganador {
		BLANCAS(0), NEGRAS(1), EMPATE(2), NADIE(3);

		private Integer value;

		private Ganador(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return value;
		}

		private static final Map<Integer, Ganador> map = new HashMap<>();

		static {
			for (Ganador g : values())
				map.put(g.value, g);
		}

		public static Ganador of(Integer value) {
			return map.get(value);
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String descripcion;

	@JoinColumn(name = "id_blancas", referencedColumnName = "id")
	@ManyToOne(cascade = {
			CascadeType.PERSIST, CascadeType.MERGE
	}, optional = false, fetch = FetchType.EAGER)
	private Usuario blancas;

	@JoinColumn(name = "id_negras", referencedColumnName = "id")
	@ManyToOne(cascade = {
			CascadeType.PERSIST, CascadeType.MERGE
	}, optional = false, fetch = FetchType.EAGER)
	private Usuario negras;

	private Ganador ganador;

	@Column(name = "fecha_hora")
	private ZonedDateTime fechaHora;

	private Long duracion;

	@ElementCollection
	@CollectionTable(name = "movimientos",
			joinColumns = @JoinColumn(name = "id_partida"))
	@OrderColumn(name = "orden")
	private List<Movimiento> movimientos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Usuario getBlancas() {
		return blancas;
	}

	public void setBlancas(Usuario blancas) {
		this.blancas = blancas;
	}

	public Usuario getNegras() {
		return negras;
	}

	public void setNegras(Usuario negras) {
		this.negras = negras;
	}

	public Ganador getGanador() {
		return ganador;
	}
	
	public String getGanadorString() {
		return ganador.toString();
	}

	public void setGanador(Ganador ganador) {
		this.ganador = ganador;
	}

	public ZonedDateTime getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(ZonedDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Long getDuracion() {
		return duracion;
	}

	public void setDuracion(Long duracion) {
		this.duracion = duracion;
	}

	public List<Movimiento> getMovimientos() {
		return movimientos;
	}

	public void setMovimientos(List<Movimiento> movimientos) {
		this.movimientos = movimientos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Partida other = (Partida) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "Partida [" + (id != null ? "id=" + id + ", " : "")
				+ (descripcion != null ? "descripcion=" + descripcion + ", " : "")
				+ (blancas != null ? "blancas=" + blancas + ", " : "")
				+ (negras != null ? "negras=" + negras + ", " : "")
				+ "ganador=" + getGanadorString() + ", "
				+ (fechaHora != null ? "fechaHora=" + fechaHora + ", " : "")
				+ (duracion != null ? "duracion=" + duracion + ", " : "")
				+ "]";
	}

}
