package ar.edu.unlu.cursos.spring.damas.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.unlu.cursos.spring.damas.entity.Usuario;

public interface UsuariosRepository extends JpaRepository<Usuario, Long> {
	
	public List<Usuario> findAllByOrderByNombreAsc();

	public Optional<Usuario> findByNombre(String username);
}
