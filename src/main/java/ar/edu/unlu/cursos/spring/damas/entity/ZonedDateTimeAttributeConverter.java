package ar.edu.unlu.cursos.spring.damas.entity;

import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ZonedDateTimeAttributeConverter
		implements AttributeConverter<ZonedDateTime, Timestamp> {

	@Override
	public Timestamp convertToDatabaseColumn(ZonedDateTime attribute) {
		// Guardar en UTC
		return attribute == null ? null
				: Timestamp.valueOf(
						attribute.withZoneSameInstant(ZoneId.of("UTC"))
								.toLocalDateTime());
	}

	@Override
	public ZonedDateTime convertToEntityAttribute(Timestamp dbData) {
		// Levantar en local
		return dbData == null ? null
				: dbData.toLocalDateTime().atZone(ZoneId.of("UTC"))
						.withZoneSameInstant(ZoneId.systemDefault());
	}

}
