package ar.edu.unlu.cursos.spring.damas.entity;

import javax.persistence.Basic;
import javax.persistence.Embeddable;

@Embeddable
public class Movimiento {

	public Movimiento() {
		super();
	}
	
	public Movimiento(String movimiento) {
		super();
		this.movimiento = movimiento;
	}

	@Basic(optional = false)
	private String movimiento;

	public String getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((movimiento == null) ? 0 : movimiento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Movimiento other = (Movimiento) obj;
		if (movimiento == null) {
			if (other.movimiento != null) return false;
		} else if (!movimiento.equals(other.movimiento)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "Movimiento ["
				+ (movimiento != null ? "movimiento=" + movimiento + ", " : "")
				+ "]";
	}
}
