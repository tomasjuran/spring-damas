package ar.edu.unlu.cursos.spring.damas.web;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import ar.edu.unlu.cursos.spring.damas.entity.AuditableBaseEntity;
import ar.edu.unlu.cursos.spring.damas.entity.Usuario;

public class UsuarioForm extends AuditableBaseEntity
		implements PasswordValidatedForm {

	private Long id;

	@Size(min = 4, max = 48)
	private String nombre;

	@NotBlank
	@Email
	private String email;

	@Size(min = 6, max = 48)
	private String password;

	@Size(min = 6, max = 48)
	private String confirmPassword;

	public UsuarioForm() {
	}

	public UsuarioForm(Usuario usuario) {
		this.id = usuario.getId();
		this.nombre = usuario.getNombre();
		this.email = usuario.getEmail();
		this.password = usuario.getContrasena();
		this.confirmPassword = usuario.getContrasena();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public boolean isNuevo() {
		return id == null;
	}
}
