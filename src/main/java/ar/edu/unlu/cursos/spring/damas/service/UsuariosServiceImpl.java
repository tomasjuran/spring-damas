package ar.edu.unlu.cursos.spring.damas.service;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.transaction.Transactional;

//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import ar.edu.unlu.cursos.spring.damas.entity.Usuario;
import ar.edu.unlu.cursos.spring.damas.repository.UsuariosRepository;

@Service
public class UsuariosServiceImpl implements UsuariosService {

	private final UsuariosRepository usuariosRepository;
//	private final PasswordEncoder passwordEncoder;

	@Inject
	public UsuariosServiceImpl(
//			PasswordEncoder passwordEncoder,
			UsuariosRepository usuariosRepository
			) {
		super();
		this.usuariosRepository = usuariosRepository;
//		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public List<Usuario> traerTodosPorNombreAlfabeticamente() {
		return usuariosRepository.findAllByOrderByNombreAsc();
	}

	@Override
	public Optional<Usuario> buscarId(long usuarioId) {
		return usuariosRepository.findById(usuarioId);
	}
	
	@Override
	public Optional<Usuario> buscarNombre(String usuarioNombre) {
		return usuariosRepository.findByNombre(usuarioNombre);
	}

	@Transactional
	@Override
	public void insertar(Usuario usuario) {
//		usuario.setContrasena(
//				passwordEncoder.encode(usuario.getContrasena()));
		usuariosRepository.save(usuario);
	}

	@Transactional
	@Override
	public void actualizar(Usuario usuario) {
		usuariosRepository.save(usuario);
	}

	@Transactional
	@Override
	public void eliminar(long id) {
		usuariosRepository.deleteById(id);
	}


}
