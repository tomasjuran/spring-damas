package ar.edu.unlu.cursos.spring.damas.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import ar.edu.unlu.cursos.spring.damas.entity.Usuario;

public interface UsuariosService {

	public List<Usuario> traerTodosPorNombreAlfabeticamente();

	public Optional<Usuario> buscarId(long usuarioId);
	
	public Optional<Usuario> buscarNombre(String usuarioNombre);
	
	@Transactional
	public void insertar(Usuario usuario);

	@Transactional
	public void actualizar(Usuario usuario);
	
	@Transactional
	public void eliminar(long id);
	
}
