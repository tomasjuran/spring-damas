package ar.edu.unlu.cursos.spring.damas.web;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ar.edu.unlu.cursos.spring.damas.entity.Partida;
import ar.edu.unlu.cursos.spring.damas.entity.Usuario;
import ar.edu.unlu.cursos.spring.damas.service.PartidasService;
import ar.edu.unlu.cursos.spring.damas.service.UsuariosService;

@Controller
@RequestMapping(value = "/usuarios")
public class UsuariosController {

	private final UsuariosService usuariosService;
	private final PartidasService partidasService;

	@Inject
	public UsuariosController(UsuariosService usuariosService,
			PartidasService partidasService) {
		super();
		this.usuariosService = usuariosService;
		this.partidasService = partidasService;
	}

	@RequestMapping("")
	public String usuarios(Model model) {
		List<Usuario> usuarios = usuariosService
				.traerTodosPorNombreAlfabeticamente();
		model.addAttribute("usuarios", usuarios);
		return "usuarios";
	}

	@RequestMapping("/{usuarioId}")
	public String verUsuario(Model model,
			@PathVariable("usuarioId") long usuarioId) {
		Optional<Usuario> usuario = usuariosService.buscarId(usuarioId);
		model.addAttribute("usuario", usuario.orElse(null));
		return "usuario-ver";
	}

	@RequestMapping("/nuevo")
	public String nuevoUsuario(Model model) {
		model.addAttribute("usuarioForm", new UsuarioForm());
		return "usuario-editar";
	}

	@RequestMapping(value = "/nuevo", method = RequestMethod.POST)
	public String nuevoUsuario(Model model,
			@ModelAttribute("usuarioForm") @Valid UsuarioForm usuarioForm,
			BindingResult result) {
		if (result.hasErrors()) {
			return "usuario-editar";
		}

		Usuario usuario = new Usuario();
		usuario.setNombre(usuarioForm.getNombre());
		usuario.setEmail(usuarioForm.getEmail());
		usuario.setContrasena(usuarioForm.getPassword());

		usuariosService.insertar(usuario);

		return "redirect:/usuarios/" + usuario.getId();
	}

	@RequestMapping("/{usuarioId}/editar")
	public String editarUsuario(Model model,
			@PathVariable("usuarioId") long usuarioId) {
		Usuario usuario = usuariosService.buscarId(usuarioId)
				.orElseThrow(RuntimeException::new);
		model.addAttribute("usuarioForm", new UsuarioForm(usuario));
		return "usuario-editar";
	}

	@RequestMapping(value = "/{usuarioId}/editar", method = RequestMethod.POST)
	public String editarUsuario(
			Model model,
			@PathVariable("usuarioId") long usuarioId,
			@ModelAttribute("usuarioForm") @Valid UsuarioForm usuarioForm,
			BindingResult result) {

		Usuario usuario = usuariosService.buscarId(usuarioId)
				.orElseThrow(RuntimeException::new);

		if (result.hasErrors()) {
			usuarioForm.setId(usuarioId);
			return "usuario-editar";
		}

		usuario.setNombre(usuarioForm.getNombre());

		usuariosService.actualizar(usuario);

		return "redirect:/usuarios/" + usuario.getId();
	}

	@RequestMapping(value = "/{usuarioId}/eliminar")
	public String eliminarUsuario(Model model,
			@PathVariable("usuarioId") long usuarioId) {

		Usuario usuario = usuariosService.buscarId(usuarioId)
				.orElseThrow(RuntimeException::new);

		List<Partida> partidas = partidasService.buscarPorUsuario(usuario);

		model.addAttribute("usuario", usuario);
		model.addAttribute("partidas", partidas);
		return "usuario-eliminar";
	}

	@RequestMapping(value = "/{usuarioId}/eliminar",
			method = RequestMethod.POST)
	public String eliminarUsuario(@PathVariable("usuarioId") long usuarioId) {
		usuariosService.buscarId(usuarioId).orElseThrow(RuntimeException::new);
		usuariosService.eliminar(usuarioId);
		return "redirect:/usuarios/";
	}

}
