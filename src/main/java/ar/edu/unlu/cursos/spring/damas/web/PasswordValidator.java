package ar.edu.unlu.cursos.spring.damas.web;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class PasswordValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return PasswordValidatedForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
				"password.required", "Password is required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword",
				"password.required.confirm",
				"Password confirmation is required");
		PasswordValidatedForm form = (PasswordValidatedForm) target;
		if (!form.getPassword().equals(form.getConfirmPassword())) {
			errors.rejectValue("password", "password.nomatch", "Passwords must match");
		}
	}

}
