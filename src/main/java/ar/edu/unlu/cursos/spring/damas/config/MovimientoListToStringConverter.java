package ar.edu.unlu.cursos.spring.damas.config;

import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import ar.edu.unlu.cursos.spring.damas.entity.Movimiento;

@Component
public class MovimientoListToStringConverter implements Converter<List<Movimiento>, String> {

	@Override
	public String convert(List<Movimiento> source) {
		String result = "";
		for (Movimiento m : source) {
			result += m.getMovimiento() + " ";
		}
		return result.trim();
	}
	
}
