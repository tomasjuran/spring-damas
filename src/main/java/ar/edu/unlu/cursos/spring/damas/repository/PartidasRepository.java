package ar.edu.unlu.cursos.spring.damas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.unlu.cursos.spring.damas.entity.Partida;
import ar.edu.unlu.cursos.spring.damas.entity.Usuario;

public interface PartidasRepository extends JpaRepository<Partida, Long> {

	List<Partida> findAllByOrderByFechaHoraDesc();
	
//	List<Partida> findByFechaHoraBefore(ZonedDateTime fechaHora);
//
//	List<Partida> findByFechaHoraAfter(ZonedDateTime fechaHora);
//
//	List<Partida> findByDuracionLessThan(long duracion);
//
//	List<Partida> findByDuracionGreaterThan(long duracion);

	List<Partida> findByBlancasOrNegras(Usuario blancas, Usuario negras);

//	int countByMovimientos(Partida partida);

}
