insert into usuarios (id, nombre, email, contrasena, created_on, modified_on)
values	(1, 'luciano', 'luchi0129@hotmail.com', 'luli1997', '2018-12-07 21:14:32', '2018-12-07 21:14:32'),
	(2, 'carolina', 'caro0807@gmail.com', 'caro2003', '2018-12-07 21:14:32', '2018-12-07 21:14:32'),
	(3, 'matias', 'mati0926@yahoo.com.ar', 'mati1992', '2018-12-07 21:14:32', '2018-12-07 21:14:32'),
	(4, 'tomito', 'tomi0629@aol.com', 'tomi1996', '2018-12-07 21:14:32', '2018-12-07 21:14:32'),
	(5, 'monica', 'moni0518@hotmail.com', 'moni1966', '2018-12-07 21:14:32', '2018-12-07 21:14:32'),
	(6, 'brujua', 'brujua@gmail.com', 'brujita95', '2018-12-07 21:14:32', '2018-12-07 21:14:32');


insert into partidas (id, descripcion, fecha_hora, duracion, id_blancas, id_negras, ganador, created_on, modified_on)
values	(1, 'Partida jugada en París', '2017-06-20 01:27:00', 600, 4, 3, 0, '2018-12-07 21:14:32', '2018-12-07 21:14:32'),
	(2, 'Partida guardada', '2017-06-19 21:27:00', 0, 3, 1, 2, '2018-12-07 21:14:32', '2018-12-07 21:14:32'),
	(3, 'Partida vieja', '2009-01-20 16:40:00', 1042, 2, 1, 1, '2018-12-07 21:14:32', '2018-12-07 21:14:32'),
	(4, 'Partida en curso', '2018-12-08 21:41:00', 592, 6, 5, 3, '2018-12-07 21:14:32', '2018-12-07 21:14:32');

insert into movimientos (id_partida, orden, movimiento)
values	(1, 0, 'c3-b4'),
	(1, 1, 'b6-c5'),
	(1, 2, 'e3-d4'),
	(1, 3, 'c5-e3'),
	(4, 0, 'b6-c5'),
	(4, 1, 'c3-d4'),
	(4, 2, 'd6-e5'),
	(4, 3, 'd4-b6'),
	(4, 4, 'c7-d6'),
	(4, 5, 'a3-b4'),
	(4, 6, 'a7-c5'),
	(4, 7, 'c5-a3');
