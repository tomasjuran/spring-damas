create table usuarios (
  id		bigint		not null,
  created_on	timestamp	not null,
  modified_on	timestamp	not null,
  nombre	varchar(64)	not null,
  email      	varchar(255)	not null,
  contrasena	varchar(64)	not null,
  primary key (id)
);

alter table usuarios
  add constraint UK_usuarios_email unique (email);

create table partidas (
  id		bigint		not null,
  created_on	timestamp	not null,
  modified_on	timestamp	not null,
  descripcion	varchar(500),
  fecha_hora	timestamp,
  duracion	bigint,
  id_blancas	bigint		not null,
  id_negras	bigint		not null,
  ganador	int(1),
  primary key (id)
);

alter table partidas
  add constraint FK_usuarios_blancas foreign key (id_blancas) references usuarios;

alter table partidas
  add constraint FK_usuarios_negras foreign key (id_negras) references usuarios;

create table movimientos (
  id_partida	bigint		not null,
  orden		int		not null,
  movimiento	varchar(5)	not null,
  primary key (id_partida, orden)
);

alter table movimientos
  add constraint FK_partidas foreign key (id_partida) references partidas;

create sequence hibernate_sequence
  start with 100
  increment by 1;
