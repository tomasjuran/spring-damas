"use_strict";

var CheckersPlayer = {

// Ids
containerId: "",
boardId: "checkers-player-board",
statusId: "checkers-player-status",
btnPrevId: "checkers-player-nav-btn-prev",
btnNextId: "checkers-player-nav-btn-next",

// Movimientos o jugadas
moves: [],

State: function() {
	// Contenedores
	this.status = null;
	// Celdas del tablero
	this.cells = [];
	// Movimientos del arreglo
	this.currentMove = -1;

	this.copyState = function(state) {
		this.status = state.status.cloneNode(true);
		var cells = [];
		state.cells.forEach(function(cell) {
			cells.push(cell.cloneNode(true));
		});
		this.cells = cells;
		this.currentMove = state.currentMove;
	}
},

state: null,
history: [],

init: function(containerId, moves) {
	var main = document.getElementById(containerId);
	this.containerId = containerId;
	this.state = new this.State();
	this.moves = this.parseMoves(moves);
	main.appendChild(this.createBoard());
	main.appendChild(this.createNavigation());
},

parseMoves: function(moves) {
	return moves.split(" ");
},

createBoard: function() {
	var board = document.createElement("table"),
			cells = [];

	for (let i = 0; i < 8; i++) {
		let row = document.createElement("tr");
		
		for (let j = 0; j < 8; j++) {
			cellClass = (i + j) % 2 == 0
					? "checkers-player-cell-dark"
					: "checkers-player-cell-light";

			var cell = document.createElement("td");
			cell.className = cellClass;
			row.appendChild(cell);
			cells.push(cell);
		}

		board.insertBefore(row, board.firstChild);
	}

	this.state.cells = cells;

	this.setLabels(board);
	this.setInitialCheckers();
	board.id = this.boardId;
	return board;
},

setLabels: function(board) {
	var tr = document.createElement("tr"),
			childs = board.children;

	tr.appendChild(document.createElement("td"));
	for (let char = 97; char < 105; char++) {
		var td = document.createElement("td");
		td.innerHTML = String.fromCharCode(char);
		td.className = "checkers-player-label";
		tr.appendChild(td);
	}
	board.appendChild(tr);

	for (let i = 1; i <= 8; i++) {
		var row = childs[8-i],
				td = document.createElement("td");
		td.innerHTML = i;
		td.className = "checkers-player-label";
		row.insertBefore(td, row.firstChild);
	}
},

setInitialCheckers: function() {
	var light = document.createElement("img"),
			dark = document.createElement("img");

	light.src = "/img/checkers_l.svg";
	light.alt = "light";
	dark.src = "/img/checkers_d.svg";
	dark.alt = "dark";

	for (let i = 0; i < 3; i++) {
		for (let j = i % 2; j < 8; j = j + 2) {
			this.state.cells[i*8+j].appendChild(light.cloneNode(true));
		}
	}

	for (let i = 5; i < 8; i++) {
		for (let j = i % 2; j < 8; j = j + 2) {
			this.state.cells[i*8+j].appendChild(dark.cloneNode(true));
		}
	}
},

createNavigation: function() {
	var navGame = document.createElement("div"),
			status = document.createElement("p"),
			pButtons = document.createElement("p"),
			btnRestart = document.createElement("button"),
			btnNext = document.createElement("button"),
			btnPrev = document.createElement("button");

	status.id = this.statusId;
	status.className = "checkers-player-status";
	status.innerHTML = window.firstMove;

	btnRestart.innerHTML = "&lt;|";
	btnRestart.className = "btn btn-success checkers-player-nav-btn";
	btnRestart.addEventListener("click", function() {
		CheckersPlayer.restartPlayer();
	});

	btnPrev.innerHTML = "&lt;";
	btnPrev.id = this.btnPrevId;
	btnPrev.className = "btn btn-success checkers-player-nav-btn";
	btnPrev.addEventListener("click", function() {
		CheckersPlayer.previousMove();
	});
	btnPrev.disabled = true;
	
	btnNext.innerHTML = "&gt;";
	btnNext.id = this.btnNextId;
	btnNext.className = "btn btn-success checkers-player-nav-btn";
	btnNext.addEventListener("click", function() {
		CheckersPlayer.nextMove();
	});

	pButtons.className = "checkers-player-nav";
	pButtons.appendChild(btnRestart);
	pButtons.appendChild(btnPrev);
	pButtons.appendChild(btnNext);

	navGame.className = "checkers-player-nav";
	navGame.appendChild(status);
	navGame.appendChild(pButtons);

	this.state.status = status;

	return navGame;
},

restartPlayer: function() {
	this.state = new this.State();
	this.createBoard();
	this.createNavigation();
	this.history = [];
	this.updateView();
},

previousMove: function() {
	if (this.history.length > 0) {
		var previousState = this.history.pop();
		this.state.copyState(previousState);
		this.updateView();
	}
},

nextMove: function() {
	if (this.state.currentMove >= this.moves.length-1)
		return;
	var previousState = this.savePreviousState(),
			nextMove,
			orig,
			dest,
			img;
	this.state = new this.State()
	this.state.copyState(previousState);
	// Get next move
	this.state.currentMove++;
	nextMove = this.getMove(this.moves[this.state.currentMove]);
	orig = this.getCellArrayPosition(nextMove[0]);
	dest = this.getCellArrayPosition(nextMove[1]);
	img = this.state.cells[orig].firstChild.cloneNode(true);
	this.state.cells[orig].removeChild(this.state.cells[orig].firstChild);
	this.state.cells[dest].appendChild(img);
	this.checkEaten(orig, dest);

	this.state.status.innerHTML = window.lastMove + " " + nextMove[0] + "-" + nextMove[1];
	this.updateView();
},

savePreviousState: function() {
	this.history.push(this.state);
	return this.state;
},

updateView: function() {
	var boardChildren = document.getElementById(this.boardId).childNodes,
			status = document.getElementById(this.statusId),
			btnPrev = document.getElementById(this.btnPrevId),
			btnNext = document.getElementById(this.btnNextId);

	for (let i = 7; i >= 0; i--) {
		let row = boardChildren[i],
				rowChildren = row.childNodes;
		for (let j = 1; j <= 8; j++) {
			let td = rowChildren[j],
					replace = this.state.cells[(7-i)*8 + j-1];
			if (td.hasChildNodes() || replace.hasChildNodes())
				if (td.hasChildNodes() != replace.hasChildNodes())
					row.replaceChild(replace, td);
				else
					td.firstChild.src = replace.firstChild.src;
		}
	}
	status.parentNode.replaceChild(this.state.status, status);
	
	if (this.state.currentMove < 0)
		btnPrev.disabled = true;
	else
		btnPrev.disabled = false;

	if (this.state.currentMove >= this.moves.length-1)
		btnNext.disabled = true;
	else
		btnNext.disabled = false;
},

getMove: function(move) {
	return move.split("-");
},

getCellArrayPosition: function(str) {
	return str.charCodeAt() - 97 + (str.charCodeAt(1) - 49) * 8;
},

checkEaten: function(orig, dest) {
	if (Math.abs(orig - dest) <= 9)
		return;
	var yDir = orig < dest ? 1 : -1,
			xDir = orig % 8 < dest % 8 ? 1 : -1;
	while (orig != dest) {
		if (this.state.cells[orig].hasChildNodes()) {
			this.state.cells[orig].removeChild(this.state.cells[orig].firstChild);
		}
		orig = orig + 8 * yDir + xDir;
	}
}

}