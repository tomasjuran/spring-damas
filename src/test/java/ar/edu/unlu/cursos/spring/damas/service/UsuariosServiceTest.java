package ar.edu.unlu.cursos.spring.damas.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unlu.cursos.spring.damas.entity.Usuario;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class UsuariosServiceTest {

	@Inject
	private UsuariosService usuariosService;
	
	@Test
	public void buscarNombreEnOrdenAsc() {
		List<Usuario> enOrden = usuariosService.traerTodosPorNombreAlfabeticamente();
		
		for (int i = 0; i < enOrden.size() -1; i++) {
			assertThat(enOrden.get(i).getNombre().compareTo(enOrden.get(i+1).getNombre()) <= 0);
		}
		
	}
	
	@Test
	public void buscarPorNombre() {
		String nombre = "matias";
		Optional<Usuario> matias = usuariosService.buscarNombre(nombre);
		
		assertThat(matias).isPresent();
		assertThat(matias.get().getNombre()).isEqualTo(nombre);
/*		
		Usuario pepito = new Usuario();
		pepito.setNombre("pepito");
		pepito.setContrasena("password1234");
		pepito.setEmail("pepito@mail.com");
		
		usuariosRepository.save(pepito);
		
		Optional<Usuario> encontrado = usuariosRepository.findByNombre("pepito");
		assertThat(encontrado).isPresent();
		
		assertThat(pepito).isEqualTo(encontrado.get());
		assertThat(pepito.getNombre()).isEqualTo(encontrado.get().getNombre());
*/
	}
	
}
