package ar.edu.unlu.cursos.spring.damas.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unlu.cursos.spring.damas.entity.Partida;
import ar.edu.unlu.cursos.spring.damas.entity.Usuario;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class PartidasServiceTest {
	
	@Inject
	private PartidasService partidasService;
	
	@Inject
	private UsuariosService usuariosService;
	
	@Test
	public void buscarPorUsuario() {
		Usuario user1 = usuariosService.buscarId(1L).get();
		
		List<Partida> porUsuario1 = partidasService.buscarPorUsuario(user1);
		for (Partida p : porUsuario1) {
			assertThat(p.getBlancas().equals(user1) || p.getNegras().equals(user1));
		}
	}
	
	@Test
	public void ordenFechaHora() {
		List<Partida> enOrden = partidasService.traerTodasPorFechaDescendiente();
		for (int i = 0; i < enOrden.size() -1; i++) {
			assertThat(enOrden.get(i).getFechaHora().compareTo(enOrden.get(i+1).getFechaHora()) >= 0);
		}
	}
	
	/*
	@Test
	public void compararFechas() {
		testHelper.persistir(partidasRepository, testHelper.partidaVieja());
		assertThat(partidasRepository.findByFechaHoraAfter(ZonedDateTime.now().minusSeconds(30))).isEmpty();
		
		testHelper.persistir(partidasRepository, testHelper.partidaEnCurso());
		assertThat(partidasRepository.findByFechaHoraAfter(ZonedDateTime.now().minusSeconds(30))).hasSize(1);
		
		testHelper.persistir(partidasRepository, testHelper.partidaParis());
		assertThat(partidasRepository.findByFechaHoraAfter(ZonedDateTime.now().minusSeconds(30))).hasSize(2);
	}
	*/
}
