El sitio web "damasdb.com" cuenta con una página principal, con dos links a las partidas y usuarios almacenados en la base de datos.

La cabecera del sitio permite navegar, cambiar de idioma, volver al inicio y buscar partidas por nombre de usuario.

Cuando se accede a "usuarios", se muestra una lista de los usuarios encontrados. Las acciones para cada usuario son:

- buscar partidas: Lleva a la pantalla de resultados de búsqueda de partidas para ese usuario.
- ver: muestra los detalles del usuario.
- editar: permite editar el nombre del usuario.
- eliminar: permite eliminar al usuario. No se puede eliminar un usuario si este participa en partidas cargadas en el sistema.

La página de "partidas" es análoga a la de usuarios, sans la opción de buscar partidas.

Cuando se accede a ver una partida, se muestra un tablero de juego interactivo y los detalles de la partida.
El tablero muestra una descripción del último movimiento y permite navegar hacia adelante y atrás los movimientos, como también volver al primer movimiento.